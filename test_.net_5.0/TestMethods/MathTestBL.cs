﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using project_.net_5._0.BusinessLogic;

namespace test_.net_5._0.TestMethods
{
    class MathTestBL
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestSum()
        {
            var mathBL = new MathBL();
            int result = mathBL.Sum(2, 3);
            Assert.IsTrue(result == 5);
        }
    }
}
