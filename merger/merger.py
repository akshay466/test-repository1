import os
import sys
import stat
import json
import base64
import shutil
import configparser as cp
import subprocess as sp
from pathlib import Path
from zipfile import ZipFile
from datetime import datetime
from collections import OrderedDict


# -----------------------------------------
# format the output
# -----------------------------------------
def format_output(out):
    for item in out:
        print_n_log(item.decode("utf-8").strip("\n"))


# -----------------------------------------
# print and log
# -----------------------------------------
def print_n_log(stmt=""):
    if stmt.startswith("summary:"):
        print(stmt.split("summary:", 1)[-1].strip())
    else:
        print(stmt)
    g_log_lst.append(stmt)
    l_log_lst.append(stmt)


# -----------------------------------------
# check if conflicts present during merge
# -----------------------------------------
def check_conflicts(out):
    # None --> No conflicts
    # False --> Conflict exist but auto merged - no need for manual intervention
    # True --> Conflict exist, auto merged failed - Need manual intervention
    conflict_flag = None
    for item in out:
        t_str = item.decode("utf-8").strip("\n")

        if t_str.__contains__("Merge conflict"):
            print_n_log("summary: !!! Merge Conflict Exist !!!")
            conflict_flag = False

        if t_str.__contains__("Automatic merge failed"):
            print_n_log("summary: !!! Manual intervention needed !!!")
            conflict_flag = True

    if not conflict_flag:
        if out[0].decode("utf-8").__contains__("Already up to date"):
            print_n_log("summary: !!! No Merge needed !!!")
            conflict_flag = None
        else:
            print_n_log("summary:!!! Auto Merged - No manual intervention needed !!!")
            if master_merge == "true":
                if tag_rls():
                    push_rls()
            else:
                push_rls()

    return conflict_flag


# -----------------------------------------
# check if conflicts present during merge
# -----------------------------------------
def tag_rls():
    prev_tag = get_tag()
    t_rls_to_be_merged = rls_to_be_merged.replace("/", "-")
    if prev_tag is not None:
        if prev_tag.startswith(t_rls_to_be_merged):
            if hotfix_flag == "true":
                if prev_tag.__contains__(".hotfix."):
                    t_tags = prev_tag.rsplit(".", 1)
                    curr_tag = t_tags[0] + "." + str(int(t_tags[-1]) + 1)
                else:
                    curr_tag = t_rls_to_be_merged + ".hotfix.1"
            else:
                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                print("Issue:hotfix flag is not chosen")
                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                abort_merge()
                return False
        else:
            curr_tag = t_rls_to_be_merged
    else:
        curr_tag = t_rls_to_be_merged

    print("Prev. Tag--> " + prev_tag + " Curr. Tag -->" + curr_tag)

    stmt = "git tag -a " + curr_tag + " -m tagged-on-" + process_time
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)

    stmt = "git push origin tag " + curr_tag
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)

    return True


# -----------------------------------------
# check if conflicts present during merge
# -----------------------------------------
def get_conflict_files():
    stmt = "git diff"
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    items = output[0].decode("utf-8").split("\n")

    fn_lst = []
    for item in items:
        if item.__contains__("diff --cc"):
            fn_lst.append(item.split()[-1])

    return list(set(fn_lst))


# -----------------------------------------
# get commit details for the file
# -----------------------------------------
def get_file_log(rls, fn):
    stmt = "git log -1 " + rls + " " + fn
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()

    items = output[0].decode("utf-8").split("\n")
    f_log_d = {}
    for item in items:
        sub_items = item.split(" ", 1)
        if len(sub_items) == 2:
            key = val = None
            if sub_items[0] == "commit":
                key = "commit_id"
                val = sub_items[1].split(" ", 1)[0]
            elif sub_items[0] == "":
                key = "commit_message"
                val = sub_items[1]
            elif sub_items[0][-1] == ":":
                key = sub_items[0][:-1]
                val = sub_items[1]
            f_log_d[key] = val.replace("<", "&lt").replace(">", "&gt")

    return f_log_d


# -----------------------------------------
# get the commit diff for file in branches
# -----------------------------------------
def get_file_diff(f_rls, t_rls, fn):
    stmt = "git diff " + f_rls + ":" + fn + " " + t_rls + ":" + fn
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()

    return {"diff": output[0].decode("utf-8").replace("<", "&lt").replace(">", "&gt")}


# -----------------------------------------
# write the conflict file
# -----------------------------------------
def write_file_content(rls, fn):
    stmt = "git show " + rls + ":" + fn
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()

    c_dir = os.path.join(process_dir, rls.replace("/", "~"))
    if not os.path.exists(c_dir):
        os.mkdir(c_dir)

    c_fn = os.path.join(c_dir, fn.replace("/", "~"))
    open(c_fn, "w", encoding="utf-8").write(output[0].decode("utf-8"))

    return c_fn


# -----------------------------------------
# generate rls log
# -----------------------------------------
def generate_rls_log(f_rls, t_rls):
    global l_log_lst
    log_str = ""
    for item in l_log_lst:
        if item.startswith("summary:"):
            log_str += item.split("summary:", 1)[-1].strip() + "\n"
        else:
            log_str += item + "\n"
    fn = t_rls.replace("/", "~") + "_" + f_rls.replace("/", "~") + "_rls.log"
    log_str += print_summary(l_log_lst, "no_print")
    open(os.path.join(reports_dir, fn), "w", encoding="utf-8").write(log_str)


# -----------------------------------------
# generate global log
# -----------------------------------------
def generate_global_log():
    global g_log_lst
    log_str = ""
    for item in g_log_lst:
        if item.startswith("summary:"):
            log_str += item.split("summary:", 1)[-1].strip() + "\n"
        else:
            log_str += item + "\n"
    fn = os.path.join(process_dir, "global.log")
    print_n_log("summary: log @" + fn.replace(base_dir, ""))
    log_str += print_summary(g_log_lst, "no_print")
    open(fn, "w", encoding="utf-8").write(log_str)


# -----------------------------------------
# collect conflicts in merge
# -----------------------------------------
def get_conflicts_details(f_rls, t_rls):
    fn_lst = get_conflict_files()
    abort_merge()

    rls_d = OrderedDict()
    t_f_d = OrderedDict()
    f_f_d = OrderedDict()
    for fn in fn_lst:
        tmp_d = {}
        tmp_d.update(get_file_log(t_rls, fn))
        tmp_d.update(get_file_diff(t_rls, f_rls, fn))
        t_f_d[fn] = tmp_d.copy()
        t_fn = write_file_content(t_rls, fn)

        tmp_d = {}
        tmp_d.update(get_file_log(f_rls, fn))
        tmp_d.update(get_file_diff(f_rls, t_rls, fn))
        f_f_d[fn] = tmp_d.copy()
        f_fn = write_file_content(f_rls, fn)

        t_f_d[fn].update({"bc_report": bc_report(f_fn, t_fn)})

    rls_d[t_rls] = t_f_d
    rls_d[f_rls] = f_f_d

    generate_conflicts_report(rls_d)
    generate_rls_log(f_rls, t_rls)

    return rls_d


# -----------------------------------------
# format - color diff text
# -----------------------------------------
def format_diff(diff_str):
    f_str = ""

    for item in diff_str.split("\n"):
        if item.startswith("+++") or item.startswith("---"):
            tmp_item = "<font color=white>" + item + "</font><br>"
        elif item.startswith("@@"):
            tmp_item = "<font color=cyan>" + item + "</font><br>"
        elif item.startswith("-"):
            tmp_item = "<font color=red>" + item + "</font><br>"
        elif item.startswith("+"):
            tmp_item = "<font color='16EA2C'>" + item + "</font><br>"
        else:
            tmp_item = "<font color=white>" + item + "</font><br>"

        f_str += tmp_item

    return f_str


# -----------------------------------------
# generate beyond-compare report
# -----------------------------------------
def bc_report(t_fn, f_fn):
    flag = True
    tmp_l = t_fn.rsplit(os.sep, 2)
    # report_dir = os.path.join(tmp_l[0], "reports")

    if not os.path.exists(reports_dir):
        os.mkdir(reports_dir)

    out_fn = os.path.join(reports_dir, tmp_l[1] + "_" + f_fn.rsplit(os.sep, 2)[1] + "_" + tmp_l[-1] + "_bc.html")
    stmt = "BCompare.exe @" + base_dir + "\\bc_script.txt " + t_fn + " " + f_fn + " " + out_fn + " /silent"
    print_n_log("command: " + stmt)

    try:
        sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    except FileNotFoundError:
        print_n_log("BeyondCompare not in path ... ignoring BC report !!!")
        flag = False

    if flag:
        print_n_log("BCompare report generated")
    else:
        print_n_log("Error: BCompare report not generated")

    return out_fn


# -----------------------------------------
# generate consolidated report
# -----------------------------------------
def consolidate_report(det_f, bc_f):
    html = "<html><body>"
    html += "<table style='font-family:calibri;font-size:20;color:white;padding: 15px;border-spacing: 3px'>"
    html += "<tr bgcolor='#77439A'> <td style='font-weight:bold;text-align:center'> Details Report </td> </tr>"
    html += "<tr> <td> <iframe src='" + det_f.rsplit(os.sep, 1)[
        -1] + "' width='1200' height='600'></iframe> </td> </tr>"
    html += "<tr bgcolor='#77439A'> <td style='font-weight:bold;text-align:center'> Beyond Compare Report </td> </tr>"
    html += "<tr> <td> <iframe src='" + bc_f.rsplit(os.sep, 1)[-1] + "' width='1200' height='800'></iframe> </td> </tr>"
    html += "</table>"
    html += "</body></html>"

    f_items = det_f.rsplit("_", 1)
    fn = f_items[0] + "_combined.html"
    open(fn, "w").write(html)
    return fn


# -----------------------------------------
# generate html report for all conflict files
# -----------------------------------------
def generate_conflicts_report(rls_d):
    rev_d = dict(reversed(list(rls_d.items())))
    print(rev_d)
    t_key = list(rev_d.keys())[0]
    f_key = list(rev_d.keys())[1]

    print_n_log("summary: Report Directory: " + os.path.join(reports_dir).replace(base_dir, ""))

    for fn_key, fn_val in rev_d[t_key].items():
        html = "<html>"
        html = html + "<table style='font-family:calibri;font-size:20;color:white;padding: 15px;border-spacing: 3px'>"

        html = html + "<th colspan=3 bgcolor='#F15A2C' style='padding:15px'>CONFLICT FILE: " + fn_key + "</th>"

        html = html + "<tr bgcolor='#EF4869'>"
        html = html + "<td style='font-weight:bold;'>RELEASE</td>"
        html = html + "<td>" + t_key + "</td>"
        html = html + "<td>" + f_key + "</td>"
        html = html + "</tr>"

        for c_key, c_val in fn_val.items():
            if c_key.lower() not in ["merge", "bc_report", "diff"]:
                html = html + "<tr bgcolor='#2DBDB6'>"
                html = html + "<td style='font-weight:bold'>" + c_key.upper() + "</td>"
                if c_key == "commit_id":
                    html = html + "<td>" + c_val[:7] + "</td>"
                    html = html + "<td>" + rev_d[f_key][fn_key][c_key][:7] + "</td>"
                else:
                    html = html + "<td>" + c_val + "</td>"
                    html = html + "<td>" + rev_d[f_key][fn_key][c_key] + "</td>"
                html = html + "</tr>"
            elif c_key.lower() == "diff":
                html = html + "<tr bgcolor='#2750A3'>"
                html = html + "<td style='font-weight:bold'>" + c_key.upper() + "</td>"
                html = html + "<td>" + format_diff(c_val) + "</td>"
                html = html + "<td>" + format_diff(rev_d[f_key][fn_key][c_key]) + "</td>"
                html = html + "</tr>"

        html = html + "</table>"
        html = html + "</html>"

        bc_fn = rls_d[f_key][fn_key]["bc_report"]
        fn = bc_fn.split("_bc.html")[0] + "_details.html"
        open(fn, "w", encoding="utf-8").write(html)
        print_n_log("summary: diff: " + fn.rsplit(os.sep, 1)[-1])
        print_n_log("summary: bcmp: " + bc_fn.rsplit(os.sep, 1)[-1])
        rls_d[f_key][fn_key].update({"diff_report": fn})

        combined_fn = consolidate_report(fn, bc_fn)
        print_n_log("summary: cons: " + combined_fn.rsplit(os.sep, 1)[-1])
        rls_d[f_key][fn_key].update({"combined_report": combined_fn})


# -----------------------------------------
# merge the release
# -----------------------------------------
def merge_rls(f_rls, t_rls):
    rls_d = {}
    stmt = "git merge " + f_rls
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)
    flag = check_conflicts(output)
    if flag:
        rls_d = get_conflicts_details(f_rls, t_rls)
    return rls_d


# -----------------------------------------
# push the release
# -----------------------------------------
def push_rls():
    stmt = "git push"
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)


# -----------------------------------------
# pull the release
# -----------------------------------------
def pull_rls():
    stmt = "git pull"
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)


# -----------------------------------------
# pull the release
# -----------------------------------------
def get_tag():
    stmt = "git describe --tags --abbrev=0"
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)
    try:
        return output[0].decode("utf-8").strip("\n")
    except IndexError:
        return None


# -----------------------------------------
# checkout the release
# -----------------------------------------
def checkout_rls(rls):
    stmt = "git checkout -B " + rls + " remotes/origin/" + rls
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)

    if output[1].decode("utf-8").__contains__("cannot be created"):
        return False

    stmt = "git branch "
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)

    pull_rls()

    return True


# -----------------------------------------
# initialize the git
# -----------------------------------------
def init_git():
    print_n_log()

    print_n_log("step 0.1".center(90, "-"))
    stmt = "git clone " + clone_url + " " + repo
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()

    if output[1].decode("utf-8").__contains__("already exists"):
        print_n_log("summary: Clone already exists")
    elif output[1].decode("utf-8").__contains__("not found"):
        print_n_log("summary: Clone url may be wrong")
        sys.exit()
    else:
        print_n_log("summary: Cloned Successfully")
        print_n_log(output[1].decode("utf-8"))

    os.chdir(repo)
    print_n_log("Switched to dir " + repo)
    print_n_log("current dir" + os.getcwd())

    print_n_log("step 0.2".center(90, "-"))
    print_n_log("summary: sync branches")
    pull_rls()
    print_n_log()


# -----------------------------------------
# load the properties file
# -----------------------------------------
def load_properties():
    props_file = "merger.properties"
    d = cp.RawConfigParser()
    d.read(props_file)
    return d


# -----------------------------------------
# get future releases
# -----------------------------------------
def get_future_releases():
    lst = []
    for item_rls in cfg_d[repo]:
        if item_rls.__contains__("future_release"):
            lst.append(cfg_d[repo][item_rls])
    return lst


# -----------------------------------------
# attach files to a jira ticket
# -----------------------------------------
def jira_add_attachments_zip(issue_key, jira_d):
    zip_obj = ZipFile('files.zip', 'w')
    for p_key, p_val in jira_d.items():
        for c_key, c_val in p_val.items():
            if os.path.exists(c_val):
                zip_obj.write(c_val, c_val.rsplit(os.sep)[-1])
            else:
                print_n_log("summary: !!!" + c_val + " is missing !!!")
    zip_obj.close()

    stmt = 'curl --url ' + jira_api_url + '/issue/' + issue_key + '/attachments' + \
           ' --user ' + jira_user + ':' + jira_token + \
           ' --request POST --header "X-Atlassian-Token: no-check" -F file=@files.zip'
    print("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    print(output)


# -----------------------------------------
# attach files to a jira ticket
# -----------------------------------------
def jira_add_attachments_files(issue_key, jira_d):
    file_str = ""
    for p_key, p_val in jira_d.items():
        for c_key, c_val in p_val.items():
            file_str += "-F file=@" + c_val + " "

        stmt = 'curl --url ' + jira_api_url + '/issue/' + issue_key + '/attachments' + \
               ' --user ' + jira_user + ':' + jira_token + \
               ' --request POST --header "X-Atlassian-Token: no-check" ' + file_str
        print("command: " + stmt)
        output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
        print(output)


# -----------------------------------------
# organize text for jira
# -----------------------------------------
def organize_text(f_rls, t_rls, fn):
    f_r = f_rls.replace("/", "~")
    t_r = t_rls.replace("/", "~")

    t_log = []
    [t_log.append(item.replace("|", "").strip()) for item in open(os.path.join(reports_dir, fn), "r").readlines()]
    log = "\\n".join(t_log)

    from_rls = os.path.join(process_dir, f_r)
    to_rls = os.path.join(process_dir, t_r)
    log = [log.replace('"', "").replace(from_rls + "\\", "").replace(to_rls + "\\", "")
               .replace(reports_dir + "\\", "").replace(process_dir + "\\", "").replace(base_dir + "\\", "")
               .replace("\\\\", "\\").replace("\n", "\\n")]

    return log


# -----------------------------------------
# add comments to a jira ticket
# -----------------------------------------
def jira_add_comments(f_rls, t_rls, issue_key):
    fn = t_rls.replace("/", "~") + "_" + f_rls.replace("/", "~") + "_rls.log"

    stmt = 'curl --url ' + jira_api_url + '/issue/' + issue_key + '/comment' + \
           ' --user ' + jira_user + ':' + jira_token + \
           ' --request POST --header "Content-Type: application/json" ' + \
           ' --data ' + open(os.path.join(base_dir, "comment.json"), "r").readline() \
               .replace("{comment}", organize_text(f_rls, t_rls, fn)[0])

    print("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    print(output)


# -----------------------------------------
# get jira account id
# -----------------------------------------
def get_jira_account_id(email):
    stmt = 'curl --url ' + jira_api_url + '/user/search?query=' + email + \
           ' --user ' + jira_user + ':' + jira_token + \
           ' --request GET --header "Accept: application/json" '
    print("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()

    try:
        datastore = json.loads(output[0].decode("utf-8"))
        return datastore[0]["accountId"]
    except IndexError:
        return None


# -----------------------------------------
# create jira ticket
# -----------------------------------------
def create_jira_ticket(rls_d):
    to_rls = list(rls_d.keys())[0]
    from_rls = list(rls_d.keys())[1]
    jira_d = prep_jira_ticket(rls_d)
    for p_key, p_val in jira_d.items():
        email = p_key.split("&lt", 1)[-1].split("&gt")[0]
        files = ", ".join(p_val.keys())

        jira_track_format = to_rls + "_" + from_rls + "_" + email + "_" + files

        if not is_jira_ticket_needed(jira_track_format):
            continue

        account_id = get_jira_account_id(email)
        summary = "Auto: " + from_rls + " to " + to_rls + " merge conflict ticket"
        desc = "Conflicts occured when merging commits from " + from_rls + " to " + to_rls + \
               ".\\nConflicts exist in these files: "

        if account_id:
            stmt = 'curl --url ' + jira_api_url + '/issue --user ' + jira_user + ':' + jira_token + \
                   ' --header "Content-Type: application/json" --data ' + \
                   open(os.path.join(base_dir, "issue.json"), "r") \
                       .readline().replace("{key}", jira_project_key) \
                       .replace("{summary}", summary) \
                       .replace("{accountId}", account_id) \
                       .replace("{description}", desc) \
                       .replace("{files}", files)
            print("command: " + stmt)
            output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()

            try:
                datastore = json.loads(output[0].decode("utf-8"))
                issue_key = datastore["key"]
                print_n_log("summary: JIRA issue created with key " + issue_key)
                print_n_log("summary: URL " + jira_project_url + "/" + jira_project_key + "/issues/" + issue_key)

                if attach_as_zip == "true":
                    print_n_log("summary: Attached as Zip")
                    jira_add_attachments_zip(issue_key, p_val)
                else:
                    print_n_log("summary: Attached as Individual Files")
                    jira_add_attachments_files(issue_key, p_val)

                jira_add_comments(from_rls, to_rls, issue_key)
                open(repo_dir + os.sep + "jira_tickets.log", "a").write(jira_track_format + "=" + issue_key + "\n")
            except IndexError:
                return None
        else:
            print("unable to create jira ticket ... since account id not found")


# -----------------------------------------
# prepare jira ticket
# -----------------------------------------
def prep_jira_ticket(rls_d):
    # print(rls_d)
    first_key = list(rls_d.keys())[0]
    d = rls_d[first_key]
    jira_d = {}
    for p_key, p_val in d.items():
        found_flag = False
        f_lst = p_val["bc_report"].rsplit(os.sep, 1)
        log_fn = f_lst[0] + os.sep + f_lst[-1].split("_")[0] + "_" + f_lst[-1].split("_")[1] + "_rls.log"
        for c_key, c_val in p_val.items():
            if c_key.lower() == "author":
                found_flag = True
                if c_val not in jira_d:
                    jira_d[c_val] = {p_key: {"bc_report": p_val["bc_report"], "diff_report": p_val["diff_report"],
                                             "log_report": log_fn, "combined_report": p_val["combined_report"]}}
                else:
                    jira_d[c_val].update(
                        {p_key: {"bc_report": p_val["bc_report"], "diff_report": p_val["diff_report"],
                                 "log_report": log_fn, "combined_report": p_val["combined_report"]}})
            if found_flag:
                break
    # print(jira_d)
    return jira_d


# -----------------------------------------
# check if ticket already exists
# -----------------------------------------
def is_jira_ticket_needed(line_format):
    try:
        lst = [item for item in open(repo_dir + os.sep + "jira_tickets.log", "r").readlines() if
               item.split("=")[0] == line_format]

        if lst:
            issue_key = lst[0].rsplit("=", 1)[-1].strip("\n")
            stmt = 'curl --url ' + jira_api_url + '/issue/' + issue_key + '?fields=status' + \
                   ' --user ' + jira_user + ':' + jira_token + \
                   ' --request GET --header "Accept: application/json " '
            print("command: " + stmt)
            output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
            print(output[0].decode("utf-8"))
            try:
                status = json.loads(output[0].decode("utf-8"))["fields"]["status"]["name"]
                if status.upper() != "DONE":
                    print_n_log("summary: JIRA Ticket already exists: " + issue_key + " ==> " + status)
                    print_n_log("summary: URL " + jira_project_url + "/" + jira_project_key + "/issues/" + issue_key)
                    return False
                else:
                    lst = [item for item in open(repo_dir + os.sep + "jira_tickets.log", "r").readlines() if
                           item.split("=")[0] != line_format]
                    open(repo_dir + os.sep + "jira_tickets.log", "w").writelines(lst)
                    return True
            except IndexError:
                return None
        else:
            return True
    except FileNotFoundError:
        print_n_log("summary: jira_tickets.log not found. Treating as first iteration")
        open(repo_dir + os.sep + "jira_tickets.log", "w")

    return True


# -----------------------------------------
# abort the merge
# -----------------------------------------
def abort_merge():
    stmt = "git merge --abort"
    print_n_log("command: " + stmt)
    output = sp.Popen(stmt, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    format_output(output)
    print_n_log("summary: Aborted Merge")


# -----------------------------------------
# propagate releases process
# -----------------------------------------
def propagate_release():
    for index, from_rls in enumerate(all_rls):
        global l_log_lst
        l_log_lst = []
        try:
            to_rls = all_rls[index + 1]

            print_n_log()
            print_n_log("".center(90, "*"))
            print_n_log("summary: Merge process for " + from_rls + " to " + to_rls)

            print_n_log("".center(90, "*"))
            print_n_log()

            print_n_log("step 1".center(90, "-"))
            flag = checkout_rls(from_rls)
            if not flag:
                print_n_log("summary: Branch " + from_rls + " does not exist !!! ... Skipping propagation ...")
                continue

            print_n_log("step 2".center(90, "-"))
            flag = checkout_rls(to_rls)
            if not flag:
                print_n_log("summary: Branch " + to_rls + " does not exist !!! ... Skipping propagation ...")
                continue

            print_n_log("step 3".center(90, "-"))
            rls_d = merge_rls(from_rls, to_rls)

            if rls_d:
                print_n_log("step 4".center(90, "-"))
                print_n_log("Prepare JIRA Ticket ...")
                create_jira_ticket(rls_d)

        except IndexError:
            print_n_log("End of release merge")

    generate_global_log()

    clean_work_folder()


# -----------------------------------------
# make read write and delete
# -----------------------------------------
def clean_work_folder():
    lst = [str(item) for item in sorted(filter(os.path.isdir, Path(repo_dir).iterdir()), key=os.path.getmtime) if
           str(item).rsplit(os.sep, 1)[-1][0].isdigit() and str(item).rsplit(os.sep, 1)[-1][1].isdigit()]

    del_lst = lst[:int(work_dir_cnt) * -1]

    for item in del_lst:
        shutil.rmtree(item)
    print_n_log("Removed folders:" + str(del_lst))


# -----------------------------------------
# make read write and delete
# -----------------------------------------
def del_rw(action, name, exc):
    os.chmod(name, stat.S_IWRITE)
    os.remove(name)


# -----------------------------------------
# print summary
# -----------------------------------------
def print_summary(lst=None, type="print_only"):
    if type == "print_only":
        if lst is None:
            lst = g_log_lst

        print("-".center(90, "-"))
        for log in lst:
            if log.startswith("summary:"):
                t_log = log.split("summary:", 1)[-1].strip()
                if t_log.startswith("Merge"):
                    print("-".ljust(90, "-"))
                    print(("|**" + t_log + "**").ljust(89, " ") + "|")
                elif t_log.startswith("diff:") or t_log.startswith("bcmp:") or t_log.startswith("cons:"):
                    print(("|---" + t_log).ljust(89, " ") + "|")
                else:
                    print(("|--" + t_log).ljust(89, " ") + "|")
        print("-".center(90, "-"))
        return None
    else:
        log_str = ""
        log_str += "-".center(90, "-") + "\n"
        for log in lst:
            if log.startswith("summary:"):
                t_log = log.split("summary:", 1)[-1].strip()
                if t_log.startswith("Merge"):
                    log_str += "-".ljust(90, "-") + " \n"
                    log_str += ("|**" + t_log + "**").ljust(89, " ") + "|" + "\n"
                elif t_log.startswith("diff report") or t_log.startswith("bcmp report"):
                    log_str += ("|----" + t_log).ljust(89, " ") + "|" + "\n"
                else:
                    log_str += ("|--" + t_log).ljust(89, " ") + "|" + "\n"
        log_str += "-".center(90, "-") + "\n"
        return log_str


# -----------------------------------------
# main process
# -----------------------------------------
cfg_d = load_properties()

master_merge = rls_to_be_merged = hotfix_flag = ""
try:
    master_merge = sys.argv[1]
except IndexError:
    print("--->master_merge param is not found")

if master_merge == "true":
    try:
        hotfix_flag = sys.argv[2]
    except IndexError:
        print("--->hotfix flag is not found")

    if hotfix_flag == "false":
        print("-->Sprint Merge, NOT hotfix Merge")
    else:
        print("-->Hotfix Merge, NOT sprint Merge")

    try:
        rls_to_be_merged = sys.argv[3]
    except IndexError:
        print("--->release to be merged is not found")

    if rls_to_be_merged.__contains__("RELEASE_TO_BE_MERGED"):
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        print("--->release to be merged is invalid !!! Aborting")
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        sys.exit()

bb_token = base64.b64decode(cfg_d.get("GENERAL", "bb_token")).decode("utf-8")
jira_api_url = cfg_d.get("GENERAL", "jira_api_url")
jira_project_url = cfg_d.get("GENERAL", "jira_project_url")
jira_user = base64.b64decode(cfg_d.get("GENERAL", "jira_user")).decode("utf-8")
jira_token = base64.b64decode(cfg_d.get("GENERAL", "jira_token")).decode("utf-8")
jira_project_key = cfg_d.get("GENERAL", "jira_project_key")
work_dir_cnt = cfg_d.get("GENERAL", "work_dir_count")
attach_as_zip = cfg_d.get("GENERAL", "attach_as_zip")
repos = cfg_d.get("GENERAL", "repos")

base_dir = os.getcwd()
process_time = datetime.now().strftime("%d%b%Y-%I%M%S%p")
for repo in repos.split(","):
    g_log_lst = l_log_lst = []
    repo = repo.strip().strip("\n")

    os.chdir(base_dir)
    repo_dir = os.path.join(base_dir, "out", repo)
    process_dir = os.path.join(base_dir, "out", repo, process_time)
    reports_dir = os.path.join(process_dir, "reports")
    if not os.path.exists(process_dir):
        os.makedirs(process_dir)

    print_n_log()
    print_n_log("".center(90, "@"))
    print_n_log(("Processing Repo:" + repo).center(90, "@"))
    print_n_log("".center(90, "@"))
    try:
        clone_url = cfg_d.get(repo, "clone_url").replace("{bb_token}", bb_token)
    except cp.NoSectionError:
        print_n_log("Properties not added for Repo " + repo)
        continue

    init_git()

    if master_merge == "true":
        all_rls = [rls_to_be_merged, "master"]
    else:
        prod_rls = cfg_d.get(repo, "prod_release")
        current_rls = cfg_d.get(repo, "current_release")
        future_rls = get_future_releases()
        all_rls = [prod_rls, current_rls] + future_rls

        print_n_log("prod_release = " + prod_rls)
        print_n_log("current_release = " + current_rls)
        print_n_log("future_release = " + str(future_rls))

    print_n_log("base_dir=" + base_dir)
    print_n_log("repo_dir=" + process_dir)
    print_n_log("process_dir=" + process_dir)
    print_n_log("reports_dir=" + reports_dir)

    print_n_log("clone_url = " + clone_url)
    print_n_log("all_releases = " + str(all_rls))

    propagate_release()

    print_summary()
